
package employee;

public abstract class StaffMember {
    protected int id;
    protected String name,address;
    
    public void setId(int id){
        this.id=id;
    }
    public int getID(){
        return id;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }
    public void setAddress(String address){
        this.address=address;
    }
    public String getAddress(){
        return address;
    }
    
    public StaffMember(int id,String name,String address){
        this.id=id;
        this.name=name;
        this.address=address;
        
    }   
    @Override
    public String toString(){
        return  "Id      : "+ id+"\n"+
                "Name    : "+ name+"\n"+
                "Address : "+address+"\n"+
                "Thank You! \n";
    }
    abstract double pay();
}
