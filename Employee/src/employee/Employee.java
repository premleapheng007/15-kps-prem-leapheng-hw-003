package employee;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Employee {

    public void mainMenu() {
        System.out.println("\n-----------------(MENU)-----------------");
        System.out.println("1 . Add Employee ");
        System.out.println("2 . Edit Emloyee");
        System.out.println("3 . Remove Employee");
        System.out.println("4 . Exit ");
        System.out.println("----------------------------------------");
    }

    public void sumMenu() {
        System.out.println("\n----------------(OPTIONS)---------------");
        System.out.println("1 . Volunteer Employee");
        System.out.println("2 . Hourly Employee");
        System.out.println("3 . Salaries Employee");
        System.out.println("4 . Back");
    }

    public String NameUpterCasde(String str) {
        String output = str.substring(0, 1).toUpperCase() + str.substring(1);
        return output;
    }

    public static boolean isNumber(String string) {
        return Pattern.matches("\\d+", string);
    }

    public static void main(String[] args) {

        boolean cheak = false, checkTure = true,
                checkhour = true, checkSalary = false, checkSala = true,
                checkIdHour = true, checkRate = true, checkEdit = true;
        int idUpdate = 0;

        Employee emp = new Employee();
        ArrayList<StaffMember> staff = new ArrayList<StaffMember>();

        staff.add(new Volunteer(1, "Kong Seanghim", "BMC"));
        staff.add(new SalariesEmployee(2, "Lav SokCheat", "BMC", 200, 100));
        staff.add(new HourlyEmployee(3, "Ket Pidor", "PP", 50, 15));

        Scanner sc = new Scanner(System.in);

        while (true) {
            Collections.sort(staff, new ComparatorSortName());

            staff.forEach((showEmp) -> {
                System.out.println(showEmp);
            });
            A:
            emp.mainMenu();
            System.out.print("=>Ples input any Opotin : ");
            String option = sc.next();

            switch (option) {

                case "1": {
                    B:
                    do {
                        emp.sumMenu();
                        System.out.print("=>Choose type of Employee : ");
                        String subOption = sc.next();
                        System.out.println("\n----------------------------------------");

                        switch (subOption) {
                            case "1":
                                int id = 0;
                                while (checkTure) {
                                    System.out.print("Input Id      : ");
                                    String StringId = sc.next();
                                    if (isNumber(StringId)) {
                                        id = Integer.parseInt(StringId);
                                        checkTure = false;
                                    } else {
                                        System.out.println("ID Can not be String or Symble!");
//                                        checkTure = false;
                                    }
                                }
                                checkTure = true;
                                System.out.print("Input Name    : ");
                                sc.nextLine();
                                String name = sc.nextLine();
                                System.out.print("Input Address : ");
                                String address = sc.nextLine();

                                staff.add(new Volunteer(id, emp.NameUpterCasde(name), address));
                                System.out.println("\n");
                                cheak = true;
                                break;

                            case "2":
                                int idHour = 0;
                                while (checkIdHour) {
                                    System.out.print("Input Id      : ");
                                    String StringId = sc.next();
                                    if (isNumber(StringId)) {
                                        idHour = Integer.parseInt(StringId);
                                        checkIdHour = false;
                                    } else {
                                        System.out.println("ID Can not be String or Symble!");
//                                        checkTure = false;
                                    }
                                }
                                checkIdHour = true;
                                System.out.print("Input Name    : ");
                                sc.nextLine();
                                String nameHour = sc.nextLine();
                                System.out.print("Input Address : ");
                                String addressHour = sc.nextLine();

                                int hours = 0;
                                while (checkhour) {
                                    System.out.print("Input Hour    : ");
                                    String StringId = sc.next();
                                    if (isNumber(StringId)) {
                                        hours = Integer.parseInt(StringId);
                                        checkhour = false;
                                    } else {
                                        System.out.println("***Hour Can not be String or Symble!");
//                                        checkTure = false;
                                    }
                                }
                                checkhour = true;
//                                System.out.print("Input Rate    : ");
//                                double rate = sc.nextDouble();

                                double rate = 0;
                                do {
                                    System.out.print("Input Bonus: ");
                                    if (sc.hasNextDouble()) {
                                        rate = sc.nextDouble();
                                        checkRate = true;
                                    } else {
                                        System.out.println("Can not be text!");
                                        checkRate = false;
                                        sc.next();
                                    }
                                } while (!checkRate);

                                staff.add(new HourlyEmployee(idHour, emp.NameUpterCasde(nameHour), addressHour, hours, rate));
                                System.out.println("\n");
                                cheak = true;
                                break;
                            case "3":
                                int idSala = 0;
                                while (checkSala) {
                                    System.out.print("Input Id      : ");
                                    String StringId = sc.next();
                                    if (isNumber(StringId)) {
                                        idSala = Integer.parseInt(StringId);
                                        checkSala = false;
                                    } else {
                                        System.out.println("ID Can not be String or Symble!");
//                                        checkTure = false;
                                    }
                                }
                                checkSala = true;
                                System.out.print("Input Name    : ");
                                sc.nextLine();
                                String nameSala = sc.nextLine();
                                System.out.print("Input Address : ");
                                String addressSala = sc.nextLine();
                                double salary = 0;
                                do {
                                    System.out.print("Input Salary  : ");
                                    if (sc.hasNextDouble()) {
                                        salary = sc.nextDouble();
                                        checkSalary = true;
                                    } else {
                                        System.out.println("Can not be text!");
                                        checkSalary = false;
                                        sc.next();
                                    }

                                } while (!checkSalary);
                                double bonus = 0;
                                do {
                                    System.out.print("Input Bonus: ");
                                    if (sc.hasNextDouble()) {
                                        bonus = sc.nextDouble();
                                        checkSalary = true;
                                    } else {
                                        System.out.println("Can not be text!");
                                        checkSalary = false;
                                        sc.next();
                                    }
                                } while (!checkSalary);
                                staff.add(new SalariesEmployee(idSala, emp.NameUpterCasde(nameSala), addressSala, salary, bonus));
                                System.out.println("\n");
                                cheak = true;
                                break;
                            case "4":
                                cheak = false;
                                continue;
                            default:
                                System.out.println("***Pleas input valid Options!");
                                System.out.println("----------------------------------------");
                                continue B;
                        }
                        Collections.sort(staff, new ComparatorSortName());

                        staff.forEach((showEmp) -> {
                            System.out.println(showEmp);
                        });
                    } while (cheak != false);
                    break;
                }

                case "2": {
                    System.out.print("Pleas input Emp ID to Edit : ");
                    idUpdate = sc.nextInt();
                }
                for (StaffMember update : staff) {
                    if (update.getID() == idUpdate) {
                        System.out.println(update);
                        if ("employee.Volunteer".equals(update.getClass().getName())) {
                            System.out.print("Input Name    : ");
                            sc.nextLine();
                            String name = sc.nextLine();
                            System.out.print("Input Address : ");
                            String address = sc.nextLine();
                            System.out.println("\n");

                            update.setName(emp.NameUpterCasde(name));
                            update.setAddress(address);

                        } else if ("employee.SalariesEmployee".equals(update.getClass().getName())) {
                            System.out.print("Input Name    : ");
                            sc.nextLine();
                            String name = sc.nextLine();
                            System.out.print("Input Address : ");
                            String address = sc.nextLine();
                            double salary = 0;
                            do {
                                System.out.print("Input Salary  : ");
                                if (sc.hasNextDouble()) {
                                    salary = sc.nextDouble();
                                    checkSalary = true;
                                } else {
                                    System.out.println("Can not be text!");
                                    checkSalary = false;
                                    sc.next();
                                }

                            } while (!checkSalary);
                            double bonus = 0;
                            do {
                                System.out.print("Input Bonus: ");
                                if (sc.hasNextDouble()) {
                                    bonus = sc.nextDouble();
                                    checkSalary = true;
                                } else {
                                    System.out.println("Can not be text!");
                                    checkSalary = false;
                                    sc.next();
                                }
                            } while (!checkSalary);
                            System.out.println("\n");

                            update.setName(emp.NameUpterCasde(name));
                            update.setAddress(address);
                            ((SalariesEmployee) update).setSalary(salary);
                            ((SalariesEmployee) update).setBonus(bonus);

                        } else if ("employee.HourlyEmployee".equals(update.getClass().getName())) {
                            System.out.print("Input Name    : ");
                            sc.nextLine();
                            String name = sc.nextLine();
                            System.out.print("Input Address : ");
                            String address = sc.nextLine();
                            int hours = 0;
                            while (checkhour) {
                                System.out.print("Input Hour    : ");
                                String StringId = sc.next();
                                if (isNumber(StringId)) {
                                    hours = Integer.parseInt(StringId);
                                    checkhour = false;
                                } else {
                                    System.out.println("***Hour Can not be String or Symble!");
//                                        checkTure = false;
                                }
                            }
                            checkhour = true;

                            double rate = 0;
                            do {
                                System.out.print("Input Bonus: ");
                                if (sc.hasNextDouble()) {
                                    rate = sc.nextDouble();
                                    checkRate = true;
                                } else {
                                    System.out.println("Can not be text!");
                                    checkRate = false;
                                    sc.next();
                                }
                            } while (!checkRate);
                            System.out.println("\n");

                            update.setName(emp.NameUpterCasde(name));
                            update.setAddress(address);
                            ((HourlyEmployee) update).setHourWorked(hours);
                            ((HourlyEmployee) update).setRate(rate);

                        }
                    }
                }
                break;

                case "3": {
                    boolean checkcon = false;
                    System.out.print("=>Input id you want to delete : ");
                    int delelteId = sc.nextInt();

                    for (StaffMember chekemp : staff) {
                        if (chekemp.getID() == delelteId) {
                            Iterator<StaffMember> itr = staff.iterator();
                            while (itr.hasNext()) {
                                StaffMember deleteEmp = itr.next();
                                if (deleteEmp.getID() == delelteId) {
                                    itr.remove();
                                    checkcon = true;
                                }
                            }
                        }
                    }
                    if (checkcon == true) {
                        System.out.println("delete suceslasdlf");
                    } else {
                        System.out.println("don't have");
                    }

                    break;
                }
                case "4": {
                    System.out.println("----------------(BYE-BYE)---------------\n");
                    System.exit(0);
                    break;
                }
                default: {
                    System.out.print("***Plase input valid number !!\n\n");
                    break;
                }
            }
        }
    }
}
